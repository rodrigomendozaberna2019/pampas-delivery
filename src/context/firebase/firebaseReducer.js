import {GET_SUCCESSFUL_ADDRESS, GET_SUCCESSFUL_PRODUCTS} from '../../types';

export default (state, action) => {
  switch (action.type) {
    case GET_SUCCESSFUL_PRODUCTS:
      return {
        ...state,
        products: action.payload,
      };
    case GET_SUCCESSFUL_ADDRESS:
      return {
        ...state,
        address: action.payload,
      };
      return state;
  }
};

import React, {useReducer} from 'react';
import firestore from '@react-native-firebase/firestore';

import FirebaseReducer from './firebaseReducer';
import FirebaseContext from './firebaseContext';

import {GET_SUCCESSFUL_ADDRESS, GET_SUCCESSFUL_PRODUCTS} from '../../types';

import _ from 'lodash';

const FirebaseState = (props) => {
  //State initial
  const initialState = {
    products: [],
    address: [],
  };

  //useReducer con dispatch para ejecutar las funciones
  const [state, dispatch] = useReducer(FirebaseReducer, initialState);

  //Get products
  const getProducts = () => {
    firestore()
      .collection('products')
      .where('existence', '==', true)
      .onSnapshot(manejarSnapshot);

    function manejarSnapshot(snapshot) {
      let products = snapshot.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data(),
        };
      });
      dispatch({
        type: GET_SUCCESSFUL_PRODUCTS,
        payload: products,
      });
    }
  };
  //Get addres user
  const getAddress = () => {
    firestore().collection('address').onSnapshot(manejarSnapshot);

    function manejarSnapshot(snapshot) {
      let address = snapshot.docs.map((doc) => {
        return {
          id: doc.id,
          ...doc.data(),
        };
      });

      dispatch({
        type: GET_SUCCESSFUL_ADDRESS,
        payload: address,
      });
    }
  };

  return (
    <FirebaseContext.Provider
      value={{
        products: state.products,
        address: state.address,
        getAddress,
        getProducts,
      }}>
      {props.children}
    </FirebaseContext.Provider>
  );
};

export default FirebaseState;

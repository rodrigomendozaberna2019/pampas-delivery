import React, {useReducer} from 'react';

import OrderReducer from './ordersReducer';
import OrderContext from './ordersContext';

import {
  SELECT_PRODUCT,
  CONFIRM_ORDER,
  SHOW_SUMMARY,
  DELETE_PRODUCT,
  ORDER_ORDERED,
} from '../../types';

const OrderState = (props) => {
  //State inicial
  const initialState = {
    order: [],
    product: null,
    total: 0,
    idOrder: '',
  };

  //useReducer con dispatch para ejecutar las funciones
  const [state, dispatch] = useReducer(OrderReducer, initialState);
  //Seleccion producto
  const selectProduct = (product) => {
    dispatch({
      type: SELECT_PRODUCT,
      payload: product,
    });
  };

  //Cuando el usuario confirma un platillo
  const saveOrder = (order) => {
    dispatch({
      type: CONFIRM_ORDER,
      payload: order,
    });
  };

  //Mueestra el total a pagar en el resumen
  const showSummary = (total) => {
    dispatch({
      type: SHOW_SUMMARY,
      payload: total,
    });
  };

  //Elimina un articulo del carrito
  const deleteProduct = (id) => {
    dispatch({
      type: DELETE_PRODUCT,
      payload: id,
    });
  };

  //
  const orderPlaced = (id) => {
    dispatch({
      type: ORDER_ORDERED,
      payload: id,
    });
  };

  return (
    <OrderContext.Provider
      value={{
        order: state.order,
        product: state.product,
        total: state.total,
        idOrder: state.idOrder,
        selectProduct,
        saveOrder,
        showSummary,
        deleteProduct,
        orderPlaced,
      }}>
      {props.children}
    </OrderContext.Provider>
  );
};

export default OrderState;

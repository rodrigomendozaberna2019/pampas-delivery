import React from 'react';

import {createDrawerNavigator} from '@react-navigation/drawer';

//Screens
import Explore from '../screens/Explore';
import CategoryList from '../screens/CategoryList';
import DetailProduct from '../screens/DetailProduct';

//Components
import DrawerContent from '../components/DrawerContent';

const Drawer = createDrawerNavigator();

export default function HomeStack() {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="Explore" component={Explore} />
      <Drawer.Screen name="CategoryList" component={CategoryList} />
      <Drawer.Screen name="DetailProduct" component={DetailProduct} />
    </Drawer.Navigator>
  );
}

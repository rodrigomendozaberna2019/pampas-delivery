import React, {useContext, useState, useEffect} from 'react';
//React Navigation
import {NavigationContainer} from '@react-navigation/native';

//Firebase
import auth from '@react-native-firebase/auth';

//Containers
import AuthStack from './AuthStack';
import HomeStack from './HomeStack';

//Context
import {AuthContext} from './AuthProvider';

//Components
import Loading from '../components/Loading';

export default function Routes() {
  const {user, setUser} = useContext(AuthContext);
  const [loading, setLoading] = useState(true);
  const [initializing, setInitializing] = useState(true);

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
    setLoading(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <NavigationContainer>
      {user ? <HomeStack /> : <AuthStack />}
    </NavigationContainer>
  );
}

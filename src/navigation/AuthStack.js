import React from 'react';

//React Navigation
import {createStackNavigator} from '@react-navigation/stack';

//Screens
import SignUp from '../screens/SignUp';
import LogIn from '../screens/LogIn';
import SplashScreen from '../screens/SplashScreen';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator initialRouteName="SplashScreen">
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{header: () => null}}
      />
      <Stack.Screen
        name="LogIn"
        component={LogIn}
        options={{header: () => null}}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{header: () => null}}
      />
    </Stack.Navigator>
  );
}

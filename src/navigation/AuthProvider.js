import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';

/**
 * This provider is created
 * to access user in whole app
 */

export const AuthContext = createContext({});

export const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsloading] = useState(false);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        isLoading,
        setIsloading,
        login: async (email, password) => {
          setIsloading(true);
          try {
            await auth().signInWithEmailAndPassword(email, password);
            setIsloading(false);
          } catch (e) {
            console.log(e);
            setIsloading(false);
          }
        },
        register: async (displayName, email, password) => {
          setIsloading(true);
          try {
            await auth()
              .createUserWithEmailAndPassword(email, password)
              .then((res) => {
                res.user.updateProfile({
                  displayName: displayName,
                });
              });
            setIsloading(false);
          } catch (e) {
            console.log(e);
            setIsloading(false);
          }
        },
        logout: async () => {
          try {
            await auth().signOut();
          } catch (e) {
            console.error(e);
          }
        },
      }}>
      {children}
    </AuthContext.Provider>
  );
};

import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';

//Super Grid
import {FlatGrid} from 'react-native-super-grid';

//Components React Native
import {
  StyleSheet,
  Text,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

function ExploreList({products}) {
  const navigation = useNavigation();

  return (
    <FlatGrid
      itemDimension={130}
      data={products}
      style={styles.gridView}
      spacing={10}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => navigation.navigate('DetailProduct')}>
          <ImageBackground
            source={{
              uri: item.image,
            }}
            style={styles.itemContainer}>
            <Text style={styles.itemName}>{item.name}</Text>
          </ImageBackground>
        </TouchableOpacity>
      )}
    />
  );
}

export default ExploreList;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 0,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    textAlign: 'center',
    borderRadius: 3,
    color: '#fff',
    fontWeight: '600',
    backgroundColor: '#d96367',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
});

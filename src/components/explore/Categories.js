import React from 'react';

//Ui React Native
import {ScrollView, StyleSheet, View, Image, Text} from 'react-native';

function Categories({categories}) {
  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      {categories.map((category, i) => {
        const {background, image, name, id} = category;
        return (
          <View style={styles.itemContainer} key={id}>
            <Image style={styles.image} source={{uri: image}} />
            <Text style={[styles.itemName, {backgroundColor: background}]}>
              {name}
            </Text>
          </View>
        );
      })}
    </ScrollView>
  );
}

export default Categories;

const styles = StyleSheet.create({
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 5,
    height: 120,
    width: 185,
  },
  itemName: {
    fontSize: 16,
    textAlign: 'center',
    color: '#fff',
    fontWeight: '600',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
});

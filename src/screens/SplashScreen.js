import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  Image,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useTheme} from '@react-navigation/native';
import {IconButton} from 'react-native-paper';

const SplashScreen = ({navigation}) => {
  const {colors} = useTheme();

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <View style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          duraton="1500"
          source={require('../assets/img/logo.png')}
          style={styles.logo}
          resizeMode="stretch"
        />
      </View>
      <Animatable.View
        style={[
          styles.footer,
          {
            backgroundColor: '#d96367',
          },
        ]}
        animation="fadeInUpBig">
        <Text
          style={[
            styles.title,
            {
              color: '#fff',
            },
          ]}>
          Haz de tu casa un restaurante!
        </Text>
        <Text style={styles.text}>
          Pide todo lo que necesitas y nosotros te lo llevamos
        </Text>
        <View style={styles.button}>
          <TouchableOpacity onPress={() => navigation.navigate('LogIn')}>
            <LinearGradient
              colors={['#ecab32', '#ecab32']}
              style={styles.signIn}>
              <Text style={styles.textSign}>Empezar</Text>
              <IconButton
                icon={{
                  uri: 'https://img.icons8.com/android/96/000000/forward.png',
                }}
                color="#ffffff"
                size={12}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

export default SplashScreen;

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 100,
    paddingHorizontal: 30,
  },
  logo: {
    width: 300,
    height: 90,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: '#fff',
    marginTop: 10,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold',
  },
});

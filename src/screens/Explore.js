import React, {useContext, useEffect, useState} from 'react';
//Ui React Native
import {View, Text, ScrollView, StyleSheet} from 'react-native';

//Components
import SearchBar from '../components/SearchBar';
import Categories from '../components/explore/Categories';
import ExploreList from '../components/explore/ExploreList';

//Firebase
import FirebaseContext from '../context/firebase/firebaseContext';
import OrderContext from '../context/order/ordersContext';

function Explore({navigation}) {
  const {products, getProducts} = useContext(FirebaseContext);
  const {selectProduct} = useContext(OrderContext);

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <View style={styles.wrapper}>
      <SearchBar navigation={navigation} products={products} />
      <ScrollView
        style={styles.scrollview}
        contentContainerStyle={styles.scrollViewContent}>
        <Text style={styles.heading}>Categorías</Text>
        <View style={styles.categories}>
          <></>
        </View>
        <Text style={styles.heading}>Explora</Text>
        <View styles={styles.explore}>
          <ExploreList products={products} />
        </View>
      </ScrollView>
    </View>
  );
}

export default Explore;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingTop: 2.5,
  },
  scrollview: {
    paddingTop: 10,
  },
  scrollViewContent: {
    paddingBottom: 70,
  },
  categories: {
    marginBottom: 30,
    paddingLeft: 5,
  },
  explore: {
    marginBottom: 30,
    paddingLeft: 15,
  },
  heading: {
    fontSize: 25,
    fontWeight: 'bold',
    paddingLeft: 15,
    paddingBottom: 20,
    color: '#000000',
  },
});

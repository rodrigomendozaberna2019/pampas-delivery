import 'react-native-gesture-handler';
import React from 'react';

//Firebase
import FirebaseState from './src/context/firebase/firebaseState';
import OrderState from './src/context/order/ordersState';

//Provider navigation
import Providers from './src/navigation';

function App() {
  return (
    <FirebaseState>
      <OrderState>
        <Providers />
      </OrderState>
    </FirebaseState>
  );
}

export default App;
